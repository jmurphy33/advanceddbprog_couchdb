﻿using LoveSeat;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CouchDB_CA
{
    public class Database
    {
        string id, rev, doc, prop, json;

        Document obj;
        CouchDatabase db;

        public Database(string username, string password)
        {
            var client = new CouchClient(username, password);


            db = client.GetDatabase("games");
        }

        // read

        public ViewResult ReadAllDocuments()
        {
            return db.GetAllDocuments();
        }

        /// <summary>
        /// Read a single document
        /// </summary>
        /// <param name="_ID"></param>
        public string ReadDocument(string _ID)
        {
            obj = db.GetDocument(_ID);
            string result = "";
            result = "Name: " + obj.Id.ToString();

            foreach (JProperty item in obj.Properties())
            {
                result = result + "\n" + item.Name + ": " + item.Value;
            }

            return result;
        }

        public string ReadProp(string doc, string property)
        {
            string propstring = "";

            Document d = db.GetDocument(doc);
            
            foreach (var prop in d.Properties())
            {
                if (prop.Name == property)
                {
                    propstring = propstring + prop;
                }
            }

            return propstring;
        }

        public string ReadCertainDocs(string searchProp, string searchVal)
        {
            var keys = db.GetAllDocuments().Keys;
            string docs = "";
            foreach (var key in keys)
            {
                Document d = db.GetDocument(key.ToString());

                var propertyFound = d.SelectToken(searchProp);

                if (propertyFound != null)
                {
                    if (propertyFound.Value<JToken>().ToString() == searchVal)
                    {
                        docs = doc + "\n"+ ReadDocument(d.Id);
                    }
                }       
            }
            return docs;
        }

        // delete



        /// <summary>
        /// Delete a single document
        /// </summary>
        /// <param name="id"></param>
        /// <param name="rev"></param>
        public void DeleteDocument(string id, string rev)
        {
            db.DeleteDocument(id, rev);
        }

        /// <summary>
        /// Delete field/property
        /// </summary>
        /// <param name="_ID"></param>
        /// <param name="property"></param>
        public void DeleteProperty(string _ID, string property)
        {
            obj = db.GetDocument(_ID);
            obj.Remove(property);
            db.SaveDocument(obj);
        }

        /// <summary>
        /// Delete field/property
        /// </summary>
        /// <param name="document"></param>
        /// <param name="property"></param>
        public void DeletePropertyInAll(string property)
        {
            var keys = db.GetAllDocuments().Keys;

            foreach (var key in keys)
            {
                Document d = db.GetDocument(key.ToString());

                foreach (var prop in d.Properties())
                {
                    if (prop.Name == property)
                    {
                        d.Remove(property);
                        break;
                    }
                }

                db.SaveDocument(d);
            }
        }


        /// <summary>
        /// Delete field/property
        /// </summary>
        /// <param name="document"></param>
        /// <param name="property"></param>
        public void DeleteAllDocs()
        {
            var keys = db.GetAllDocuments().Keys;

            foreach (var key in keys)
            {
                Document d = db.GetDocument(key.ToString());

                db.DeleteDocument(d.Id, d.Rev);

            }
        }

        public void DeleteCertainDocs(string searchProp, string searchVal)
        {

            var keys = db.GetAllDocuments().Keys;

            foreach (var key in keys)
            {
                Document d = db.GetDocument(key.ToString());

                var propertyFound = d.SelectToken(searchProp);

                if (propertyFound != null)
                {
                    if (searchVal == "")
                    {
                        db.DeleteDocument(d.Id, d.Rev);
                    }

                    else if (propertyFound.Value<JToken>().ToString() == searchVal)
                    {
                        db.DeleteDocument(d.Id, d.Rev);
                    }
                }

                db.SaveDocument(d);
            }
        }

        // UPDATE
        public void UpdateCertainDocs(string JSON, string searchProp, string searchVal)
        {
            JToken t1 = JToken.Parse(JSON);
            var keys = db.GetAllDocuments().Keys;

            foreach (var key in keys)
            {
                Document d = db.GetDocument(key.ToString());

                var propertyFound = d.SelectToken(searchProp);

                if (propertyFound != null)
                {
                    if (searchVal == "") { d.Merge(t1); }

                    else if (propertyFound.Value<JToken>().ToString() == searchVal)
                    {
                        d.Merge(t1);
                    }
                }
                db.SaveDocument(d);
            }
        }

        /// <summary>
        ///  Update single document
        /// </summary>
        /// <param name="_ID"></param>
        /// <param name="property"></param>
        /// <param name="json"></param>
        public void UpdateDocument(string _ID, string json)
        {
            JToken t1 = JToken.Parse(json);
            obj = db.GetDocument(_ID);
            obj.Merge(t1);
            db.SaveDocument(obj);
        }

        public void UpdateAllDocument(string JSON)
        {
            JToken t1 = JToken.Parse(JSON);

            var keys = db.GetAllDocuments().Keys;

            foreach (var key in keys)
            {
                Document d = db.GetDocument(key.ToString());

                d.Merge(t1);

                db.SaveDocument(d);
            }
        }

        // create

        /// <summary>
        /// Insert single document
        /// </summary>
        /// <param name="document_name"></param>
        /// <param name="json"></param>
        public void InsertDocument(string document_name, string json)
        {
            db.CreateDocument(document_name, json);
        }

        public void InsertManyDocument(Dictionary<string, string> documents)
        {
            foreach (var doc in documents)
            {
                db.CreateDocument(doc.Key, doc.Value);

            }

        }


        // map reduce

        public string MapReduceView()
        {
            db.SetDefaultDesignDoc("Publishers");
            ViewOptions options = new ViewOptions();
            options.Reduce = true;
            options.Group = true;
            ViewResult view = db.View("By_Name", options);
            return ""+view.Json;
        }
    }
}
