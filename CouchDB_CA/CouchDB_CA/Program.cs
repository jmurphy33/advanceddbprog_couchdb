﻿using System;
using System.Linq;
using LoveSeat;

// COUCHDB 
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CouchDB_CA
{
    class Program
    {

        static Database db;
        static string id, rev, doc, prop, json, json2;
        static Dictionary<string, string> documentList;
        public static void Main(string[] args)
        {
            try
            {
                startClasses();

                Console.WriteLine(" 1:\t READ DOC" +
                                "\n 2:\t READ ALL DOCS" +
                                "\n 3:\t READ CERTAIN DOCS" +
                                "\n 4:\t READ PROPERTY" +
                                "\n 5:\t DELETE DOC" +
                                "\n 6:\t DELETE PROPERTY " +
                                "\n 7:\t DELETE PROPERTY IN ALL DOCS" +
                                "\n 8:\t DELETE ALL DOCS" +
                                "\n 9:\t DELETE CERTAIN DOCS" +
                                "\n 10\t UPDATE DOC" +
                                "\n 11:\t UPDATE ALL DOCS " +
                                "\n 12:\t UPDATE CERTAIN DOCS " +
                                "\n 13:\t INSERT DOC" +
                                "\n 14:\t INSERT MANY DOCS " +
                                "\n 15:\t MAP REDUCE OF GAMES DEV " +
                                "\n------------------------------------------------------------------"
                                );
                string number = Console.ReadLine();

                if      (number == "1") { Read_Document(); }
                else if (number == "2") { Read_AllDocuments(); }
                else if (number == "3") { Read_CertainDocument(); }
                else if (number == "4") { Read_Property(); }
                else if (number == "5") { Delete_Document(); }
                else if (number == "6") { Delete_Property(); }
                else if (number == "7") { Delete_PropertyInAllDocs(); }
                else if (number == "8") { Delete_AllDocuments(); }
                else if (number == "9") { Delete_CertainDocuments(); }
                else if (number == "10") { Update_Document(); }
                else if (number == "11") { Update_AllDocuments(); }
                else if (number == "12") { Update_CertainDocument(); }
                else if (number == "13") { Create_Document(); }
                else if (number == "14") { Create_ManyDocuments(); }
                else if (number == "15") { MapReduce(); }
                else { Console.WriteLine("Not an option...ending program"); }

                Console.WriteLine("Worked...");
                Console.Read();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.Read();
            }

        }

        // C - Creates

        static void Create_Document()
        {
            Console.WriteLine("Enter a doucment to add:");
            doc = Console.ReadLine();
            Console.WriteLine("Enter a JSON for document:");
            json = Console.ReadLine();
            db.InsertDocument(doc, json);
        }

        static void Create_ManyDocuments()
        {
            documentList = new Dictionary<string, string>();
            string addAnother = "Y";
            while (addAnother.ToUpper() == "Y")
            {
                Console.WriteLine("Enter a doucment to add:");
                doc = Console.ReadLine();
                Console.WriteLine("Enter a JSON for document:");
                json = Console.ReadLine();
                documentList.Add(doc, json);

                Console.WriteLine("press Y to add another");
                addAnother = Console.ReadLine();
            }

            if (documentList.Count() != 0)
            {
                db.InsertManyDocument(documentList);
            }

        }

        // R - Reads

        static void Read_Document()
        {
            Console.WriteLine("Enter a document to search for: ");
            doc = Console.ReadLine();
            Console.WriteLine(db.ReadDocument(doc));
        }

        static void Read_Property()
        {
            Console.WriteLine("Enter a document to search for: ");
            doc = Console.ReadLine();
            Console.WriteLine("Enter a property to search for: ");
            prop = Console.ReadLine();
            Console.WriteLine(db.ReadProp(doc, prop));
        }

        static void Read_AllDocuments()
        {
            Console.WriteLine(db.ReadAllDocuments());
        }

        static void Read_CertainDocument()
        {
            string key, value;

            Console.WriteLine("Enter property to Search with:");
            key = Console.ReadLine();
            Console.WriteLine("Enter vale to Search with:");
            value = Console.ReadLine();
            Console.WriteLine(db.ReadCertainDocs(key, value));


        }

        // U - Updates   

        static void Update_AllDocuments()
        {

            Console.WriteLine("Enter JSON:");
            json = Console.ReadLine();

            db.UpdateAllDocument(json);
        }

        static void Update_Document()
        {
            //  write to a document
            Console.WriteLine("Enter a doucment to add to:");
            doc = Console.ReadLine();
            Console.WriteLine("Enter JSON:");
            json = Console.ReadLine();
            db.UpdateDocument(doc, json);
        }

        static void Update_CertainDocument()
        {
            string key, value;

            Console.WriteLine("Enter JSON to update for:");
            json = Console.ReadLine();
            Console.WriteLine("Enter property to Search with:");
            prop = Console.ReadLine();
            Console.WriteLine("Enter vale to Search with:");
            value = Console.ReadLine();
            db.UpdateCertainDocs(json, prop, value);


        }


        // D - Deletes

        static void Delete_AllDocuments()
        {
            db.DeleteAllDocs();

        }

        static void Delete_Document()
        {
            Console.WriteLine("Enter id");
            id = Console.ReadLine();
            Console.WriteLine("Enter rev");
            rev = Console.ReadLine();
            db.DeleteDocument(id, rev);
        }

        static void Delete_Property()
        {
            Console.WriteLine("Enter a doucment:");
            doc = Console.ReadLine();
            Console.WriteLine("Enter a property:");
            prop = Console.ReadLine();
            db.DeleteProperty(doc, prop);
        }

        static void Delete_PropertyInAllDocs()
        {
            Console.WriteLine("Enter a property to delete:");
            prop = Console.ReadLine();
            db.DeletePropertyInAll(prop);
        }

        static void Delete_CertainDocuments()
        {
            string key, value;

            Console.WriteLine("Enter property to Delete with:");
            prop = Console.ReadLine();
            Console.WriteLine("Enter value to Search with:");
            value = Console.ReadLine();
            db.DeleteCertainDocs(prop, value);


        }

        // Map Reduce

        static void MapReduce()
        {
            Console.WriteLine("Reading map reduce:");
            Console.WriteLine(db.MapReduceView());
        }

        // Start
        public static void startClasses()
        {

            db = new Database("jason", "1234");
        }

        /// <summary>
        /// Read all documents in the databse
        /// </summary>

    }
}
